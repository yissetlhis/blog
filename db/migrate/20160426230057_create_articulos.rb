class CreateArticulos < ActiveRecord::Migration
  def change
    create_table :articulos do |t|
      t.string :titulo
      t.string :string
      t.text :cuerpo
      t.date :fecha

      t.timestamps null: false
    end
  end
end
