json.array!(@articulos) do |articulo|
  json.extract! articulo, :id, :titulo, :string, :cuerpo, :fecha
  json.url articulo_url(articulo, format: :json)
end
